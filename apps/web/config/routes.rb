# get '/books', to: 'books#create'
# get '/books/new', to: 'books#new'
# get '/books', to: 'books#index'
# get '/', to: 'home#index'
# Configure your routes here
# See: http://www.rubydoc.info/gems/lotus-router/#Usage
resources :books
get '/', to: 'home#index', as: :home